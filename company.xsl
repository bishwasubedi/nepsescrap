<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <companies>
      <xsl:attribute name="url">
      	<xsl:value-of select="//table/tr/td/form/@action"/>
      </xsl:attribute>
      <xsl:apply-templates select="//table"/>
    </companies>
  </xsl:template>

  <xsl:template match="table">
    <xsl:for-each select="tr[count(td)&gt;=6 and not(@*)]">
      <company>
		<xsl:attribute name="url">
		  <xsl:value-of select="td[6]/a/@href"/>
		</xsl:attribute>
        <sn><xsl:value-of select="td[1]"/></sn>
        <id><xsl:value-of select="substring-after(td[6]/a/@href, 'display/')"/></id>
        <symbol><xsl:value-of select="td[4]"/></symbol>
        <name><xsl:value-of select="normalize-space(td[3])"/></name>
        <sector><xsl:value-of select="td[5]"/></sector>
      </company>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
