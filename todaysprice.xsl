<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <todaysprice>
      <xsl:attribute name="url">
      	<xsl:value-of select="//table/tr/td/form/@action"/>
      </xsl:attribute>
      <xsl:apply-templates select="//table"/>
    </todaysprice>
  </xsl:template>

  <xsl:template match="table">
	<xsl:variable name="dt" select="substring-after(tr/td/label, 'As of ')"/>
	<date>
	  <xsl:value-of select="substring-before($dt, '&#160;')"/>
	</date>
	<time>
	  <xsl:value-of select="substring-after($dt, '&#160;')"/>
	</time>
	<rows>
      <xsl:for-each select="tr[count(td)&gt;=10 and not(@*)]">
        <row>
          <sn><xsl:value-of select="td[1]"/></sn>
          <company><xsl:value-of select="td[2]"/></company>
          <last><xsl:value-of select="td[9]"/></last>
          <max><xsl:value-of select="td[4]"/></max>
          <min><xsl:value-of select="td[5]"/></min>
          <close><xsl:value-of select="td[6]"/></close>
          <diff><xsl:value-of select="substring-before(td[10], '&#160;')"/></diff>
          <trans><xsl:value-of select="td[3]"/></trans>
          <shares><xsl:value-of select="td[7]"/></shares>
          <amount><xsl:value-of select="td[8]"/></amount>
        </row>
      </xsl:for-each>
	</rows>
	<trans>
	  <xsl:value-of select="tr[contains(td, 'Transactions')]/td[2]"/>
	</trans>
	<quantity>
	  <xsl:value-of select="tr[contains(td, 'Quantity')]/td[2]"/>
	</quantity>
	<amount>
	  <xsl:value-of select="tr[contains(td, 'Amount')]/td[2]"/>
	</amount>
  </xsl:template>

</xsl:stylesheet>
