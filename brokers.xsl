<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <brokers>
      <xsl:attribute name="url">
      	<xsl:value-of select="//table/tr/td/form/@action"/>
      </xsl:attribute>
      <xsl:apply-templates select="//div[@class='container']/div[@class='row']/div[@class='col-md-4']/div[@class='content']"/>
    </brokers>
  </xsl:template>

  <xsl:template match="div">
  	<broker>
  	  <code><xsl:value-of select="normalize-space(div[2]/div)"/></code>
  	  <name><xsl:value-of select="normalize-space(h4)"/></name>
  	  <address><xsl:value-of select="normalize-space(div[1]/div)"/></address>
  	  <phone><xsl:value-of select="normalize-space(div[3]/div)"/></phone>
  	  <email><xsl:value-of select="normalize-space(div[4]/div)"/></email>
  	  <website><xsl:value-of select="normalize-space(div[5]/div)"/></website>
  	  <contact><xsl:value-of select="normalize-space(div[6]/div)"/></contact>
  	</broker>
  </xsl:template>

</xsl:stylesheet>
