NepseScrap
==========

Nepal Stock Exchange website data scrap tool.

 * Version:    1.3.1
 * Status:     Development
 * Author:     Bishwa Subedi
 * Email:      bishwasubedi@gmail.com
 * Website:    https//bishwa.net
 * Copyright:  Copyright (C) 2017, Bishwa Subedi
 * License:    Proprietary
 * Maintainer: Bishwa Subedi
 * Credits:    Shankar Bhattarai, Santosh Purbey


Usage
-----

	nepse.py [OPTIONS] ARG [FIELDS] [QUERY]

	OPTIONS:
	    -a      Fetch all (only first page by default)
	    -c      Output in CSV format (default is JSON)
	    -d      Fetch details (only for companies)
	    -f      Fetch latest floorsheet for given symbol. 
	    -h      Print this usage help and exit.
	    -p      Fetch price history for given symbol.
	    -x      Output in XML format (default is JSON)

	ARG:
	    Object name or stock symbol.

	FIELDS:
	    Output fields (field1,field2,...)

	QUERY:
	    KEY=VALUE pairs seperated by space.


Examples
--------

Get list of companies in CSV format:

	nepse.py -a company

Get list of companies in XML format:

	nepse.py -a company -x

Get company details:

	nepse.py -a company -d sn,id,stock_symbol,stock_name,address,email,sector

Get the list of brokers:

	nepse.py -a brokers

Get today's stock prices for all companies:

	nepse.py -a todaysprice

Get price history for a company given it's stock symbol:

	nepse.py -p SYMBOL

Get latest Floor Sheet for given stock symbol:

	nepse.py -f SYMBOL


Developers
----------

Example snippet:

	#!/usr/bin/env python
	
	import nepse
	
	b = nepse.get_instance('brokers')
	if b.fetch(nepse.FETCH_ALL):
		print(b.to_csv())
	
	c = nepse.get_instance('company')
	if c.fetch():
		prices = {}
		for row in c.rows:
			s = row['stock_symbol']
			p = c.get_prices(s)
			if p:
				prices[s] = p.to_xml()


Compatibility
-------------

The code is ompatible with Python 2.7 and Python 3.x.

Abbreviated Object Names
------------------------

	a:	calculation     # Trading Average Price
	b:  brokers         # List of Brokers
	c:  company         # Listed Companies
	f:  floorsheet      # Today's Floor Sheet
	i:  indices         # Datewise Indices
	p:  promoter-share	# Promoter Share
	t:  todaysprice     # Today's Share Price

For command line use only.
