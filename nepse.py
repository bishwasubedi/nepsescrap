#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""nepse.py: Nepal Stock Exchange data scrap tool.

Version:    1.3.1
Status:     Development
Author:     Bishwa Subedi
Email:      bishwasubedi@gmail.com
Website:    https//bishwa.net
Copyright:  Copyright (C) 2017, Bishwa Subedi
License:    Proprietary
Maintainer: Bishwa Subedi
Credits:    Shankar Bhattarai, Santosh Purbey
"""

from __future__ import print_function
from __future__ import unicode_literals

import sys

if sys.version_info[0] > 2:
    def lzip(*a, **kw):
        return list(zip(*a, **kw))
    str_types = str
else:
    def lzip(*a, **kw):
        return zip(*a, **kw)
    str_types = basestring

import requests

from lxml import html
from lxml import etree

try:
    import simplejson as json
except ImportError:
    import json

# Dependencies: pip install lxml requests


'''
import logging
try:
    import http.client as http_client
except ImportError:
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True
'''


__version__     = "1.3.1"
__author__      = "Bishwa Subedi"
__email__       = "bishwasubedi@gmail.com"
__copyright__   = "Copyright (C) 2017, Bishwa Subedi"
__license__     = "Proprietary"
__maintainer__  = "Bishwa Subedi"
__credits__     = ["Shankar Bhattarai", "Santosh Purbey"]
__status__      = "Development"


BASE_URL = 'http://www.nepalstock.com/'

# Strange behavior using curl or wget
# with default user-agent, requests
# works for now, but change it anyway
# to be on the safe side.
USER_AGENT = {'User-agent': 'Mozilla/5.0'}

#FIXME: Date Calculation.
PRICES_SDATE = '2010-01-01'
PRICES_EDATE = '2020-12-31'

FETCH_DEF, FETCH_ALL = 0, 500
_LIMITS = [50, 200, 300, 500]

_XPATH_ROOT = "//table"
_XPATH_FORM = "string(%s/tr/td/form/@action)"
_XPATH_HEAD = "/tr[@class='unique']/td"
_XPATH_ROWS = "/tr[count(td)>=%d and not(@*)]"
_XPATH_COLS = "td"
_XPATH_TEXT = "normalize-space()"
_XPATH_HREF = "string(%s/a/@href)" % _XPATH_COLS

_XPATH_FOOT = "%s/tr/td/div[@class='pager']/a[@href='#']"
_XPATH_FOOT = "substring-after(%s, 'Page ')" % _XPATH_FOOT

XPATH_BETWEEN = "substring-before(substring-after(%s, '%s'), '%s')"

_REMOVE_CHARS = '`~!@#$%^&*()=+[]{}\\|;:\'",.<>/?'


# Compile repeatedly used xpath.
_xp_cols = etree.XPath(_XPATH_COLS)
_xp_text = etree.XPath(_XPATH_TEXT)
_xp_href = etree.XPath(_XPATH_HREF)


def _head_text(s):
    r = _xp_text(s).replace('%', 'percent')
    r = r.replace('&amp;', 'and').replace('&', 'and')
    #FIXME: Sanitize for use in CSV header and XML tag name.
    r = ''.join(['' if c in _REMOVE_CHARS else c for c in r])
    return r.strip().replace(' ', '_').lower()

def _data_text(s):
    #FIXME: Convert HTML entities.
    return _xp_text(s).strip()


def _func_name(n=0):
    return sys._getframe(n + 1).f_code.co_name

def metaclass(mcls):
    def wrapper(cls):
        attrs = cls.__dict__.copy()
        slots = attrs.get('__slots__', None)
        if slots is not None:
            if isinstance(slots, str_types):
                slots = [slots]
            for slot in slots:
                attrs.pop(slot, None)
        attrs.pop('__dict__', None)
        attrs.pop('__weakref__', None)
        if hasattr(cls, '__qualname__'):
            attrs['__qualname__'] = cls.__qualname__
        return mcls(cls.__name__, cls.__bases__, attrs)
    return wrapper


class Meta(type):
    __ATTR_ERROR = "type object '%s' has no attribute '%s'"
    __superclass, __subclasses = None, {}
    def __init__(self, name, bases, attrs):
        if self.__superclass is None:
            self.__superclass = self
        else:
            self.__subclasses[name.lower()] = self
        super(Meta, self).__init__(name, bases, attrs)
    def get_instance(self, name, *args, **kwds):
        if self is self.__superclass:
            try:
                return self.__subclasses[name.lower()](*args, **kwds)
            except KeyError:
                pass
            return self.__superclass(name, *args, **kwds)
        e = self.__ATTR_ERROR % (self.__name__, _func_name())
        raise AttributeError(e)


@metaclass(Meta)
class Nepse(object):
    
    _txml = '<?xml version="1.0"?>\n<%s>\n%s\n</%s>\n'
    _trow, _tcol = '\t<%s>\n%s\n\t</%s>', '\t\t<%s>%s</%s>'
    
    def __init__(self, url, fields=None, sess=None, **kwds):
        self._url = url if ':' in url else BASE_URL + url
        self._sess = sess or requests.Session()
        self._xpath = kwds.get('xpath', _XPATH_ROOT)
        self.limit = kwds.get('limit', FETCH_DEF)
        self._rows, self._data = [], {}
        self._set_fields(fields)
    
    def fetch(self, limit=None, **kwds):
        self.reset()
        if limit is None:
            limit = self.limit
        if limit > 0:
            if limit in _LIMITS:
                kwds['_limit'] = limit
            else:
                limit = False
        num_cols, last_page = False, 0
        rows = []
        while True:
            tree = self._fetch(self._url, **kwds)
            if not num_cols:
                num_cols = self._init(tree)
                if not num_cols:
                    return False
            if limit:
                curr_page, num_pages = self._pages(tree)
                if curr_page > last_page:
                    num_rows = self._parse(tree, rows)
                    if num_rows and (curr_page < num_pages):
                        last_page = curr_page
                        kwds['page'] = curr_page+1
                        continue
                num_rows = len(rows)
            else:
                num_rows = self._parse(tree, rows)
            break
        if num_rows:
            self._rows = rows
        return num_rows
    
    def reset(self):
        self._rows, self._data = [], {}
    
    def result(self, output='json', fields=None, **kwds):
        func = 'to_' + output
        return getattr(self, func)(fields, **kwds)
    
    def to_csv(self, fields=None, fs=',', fd='"', nl='\n'):
        _ = lambda x: x.replace(fd, '\\' + fd)
        if not self.num_cols:
            return ''
        if fields is None:
            fields = self._fields
        if isinstance(fields[0], str_types):
            head = fields
        else:
            fields, head = lzip(*fields) 
        if self.num_rows:
            rows = []
            for row in self._rows:
                cols = [fd + _(row.get(f, '')) + fd for f in fields]
                rows.append(fs.join(cols))
            return fs.join(head) + nl + nl.join(rows) + nl
        return fs.join(head) + nl
    
    def to_json(self, fields=None, indent=4, **kwds):
        #TODO: Output only selected fields.
        if indent > 0:
            return json.dumps(self.data, indent=indent, **kwds)
        return json.dumps(self.data, **kwds)
    
    def to_xml(self, fields=None, root='rows', rec='row', nl='\n'):
        rf = '%s url="%s"' % (root, self._data['form'])
        txml = self._txml % (rf, '%s', root)
        trow = self._trow % (rec, '%s', rec)
        if self.num_rows and self.num_cols:
            tcol = self._tcol
            if fields is None:
                fields = self._fields
            if isinstance(fields[0], str_types):
                fields = lzip(fields, fields)
            rows = []
            for row in self._rows:
                cols = [tcol % (t, row.get(f, ''), t) for f, t in fields]
                rows.append(trow % '\n'.join(cols))
            r = txml % '\n'.join(rows)
        else:
            r = txml % ''
        if nl != '\n':
            return r.replace('\n', nl)
        return r
    
    @property
    def data(self):
        data = self._data.copy()
        data['rows'] = self.rows
        return data
    
    @property
    def fields(self):
        return self._fields[:]
    
    @property
    def num_cols(self):
        return self._fcount
    
    @property
    def num_rows(self):
        return len(self._rows)
    
    @property
    def rows(self):
        #return self._rows[:]
        return self._rows
    
    def _fetch(self, url, **kwds):
        if len(kwds):
            resp = self._sess.post(url, headers=USER_AGENT, data=kwds)
        else:
            resp = self._sess.get(url, headers=USER_AGENT)
        return html.fromstring(resp.content)
    
    def _get_fields(self, tree, xpath=None, func=None):
        if xpath is None:
            xpath = self._xpath + _XPATH_HEAD
        if func is None:
            func = _head_text
        return [func(f) for f in tree.xpath(xpath)]
    
    def _init(self, tree, xpath=None):
        if xpath is None:
            xpath = _XPATH_FORM % self._xpath
        self._data['form'] = tree.xpath(xpath)
        num_cols = self.num_cols
        if not num_cols:
            cols = self._get_fields(tree)
            return self._set_fields(cols)
        return num_cols
    
    def _pages(self, tree, xpath=None):
        if xpath is None:
            xpath = _XPATH_FOOT % self._xpath
        if isinstance(xpath, str_types):
            foot = tree.xpath(xpath).strip()
        else:
            foot = xpath(tree).strip()
        if '/' in foot:
            page, pages = foot.split('/')
            try:
                page, pages = int(page), int(pages)
                if (page > 0) and (pages > 0):
                    return (page, pages)
            except ValueError:
                pass
        return (1, 1)
    
    def _parse(self, tree, rows, xpath=None, func=None, xp=None, fn=None):
        if xpath is None:
            xpath = self._xpath + _XPATH_ROWS % self._fcount
        if xp is None:
            xp = _xp_cols
        if fn is None:
            fn = _data_text
        forder, fcount = self._forder, self._fcount
        if isinstance(xpath, str_types):
            _rows = tree.xpath(xpath)
        else:
            _rows = xpath(tree)
        num_rows = len(rows)
        for row in _rows:
            _cols = xp(row)
            if len(_cols) >= fcount:
                cols = {f: fn(_cols[i]) for f, i in forder}
                if func is not None:
                    func(row, cols)
                rows.append(cols)
        return len(rows) - num_rows
    
    def _set_fields(self, cols):
        if len(self._data):
            fcount = len(cols)
            if fcount:
                if len(self._fields):
                    fields = [f for f in self._fields if f]
                    forder = [(f, i) for i, f in enumerate(cols) if f and f in fields]
                else:
                    forder = [(f, i) for i, f in enumerate(cols) if f]
                    self._fields = [f for f, _ in forder]
                if len(forder):
                    self._forder, self._fcount = forder, fcount
                    return fcount
        else:
            try:
                self._fields = cols[:]
            except TypeError:
                self._fields = []
        self._forder, self._fcount = None, 0
        return False


class Brokers(Nepse):
    
    __FIELDS = [
                'address',
                'code',
                'phone',
                'email',
                'website',
                'contact',
                ]
    
    __DIVLIST = [
                "div[@class='container']",
                "div[@class='row']",
                "div[@class='col-md-4']",
                "div[@class='content']",
                ]
    
    __XP_ROWS = "//" + '/'.join(__DIVLIST)
    __XP_COLS = "div[@class='row-div']"
    __XP_NAME = "normalize-space(h4)"
    
    _FIELDS = ['name'] + __FIELDS
    
    def __init__(self, fields=None, sess=None, **kwds):
        if fields is None:
            fields = self._FIELDS
        super(Brokers, self).__init__('brokers', fields, sess, **kwds)
    
    def _get_fields(self, tree, xpath=None, func=None):
        if xpath or func:
            return super(Brokers, self)._get_fields(tree, xpath, func)
        return self.__FIELDS
    
    def _parse(self, tree, rows, xpath=None, func=None, xp=None, fn=None):
        if xpath is None:
            xpath = self.__XP_ROWS
        if xp is None:
            xp = etree.XPath(self.__XP_COLS)
        if func is None:
            xp_name = etree.XPath(self.__XP_NAME)
            def _func(row, cols):
                cols['name'] = xp_name(row)
            func = _func
        return super(Brokers, self)._parse(tree, rows, xpath, func, xp, fn)


class Company(Nepse):
    
    _XPATH = "//div[@id='company-view']/table/tr[count(td)>=2]"
    
    _FIELDS = ['sn', 'stock_name', 'stock_symbol', 'sector', 'id', 'url']
    _DETAIL = ['address', 'email', 'website', 'tls', 'puv', 'tpuv', 'ltp', 'mc']
    
    def __init__(self, fields=None, sess=None, **kwds):
        if fields is None:
            if 'detail' in kwds:
                fields = self._FIELDS + self._DETAIL
            else:
                fields = self._FIELDS
        super(Company, self).__init__('company', fields, sess, **kwds)
        self.detail = kwds.get('detail', False)
    
    def fetch(self, limit=None, detail=None, **kwds):
        if detail is None:
            detail = self.detail
        num_rows = super(Company, self).fetch(limit, **kwds)
        if detail and num_rows:
            xp = etree.XPath(self._XPATH)
            for row in self._rows:
                tree = self._fetch(row['url'])
                for r in xp(tree):
                    cols = _xp_cols(r)
                    if len(cols) >= 2:
                        k = _head_text(cols[0])
                        v = _data_text(cols[1])
                        if k and v:
                            if k.startswith('address'):
                                row['address'] = v
                            elif k.startswith('email'):
                                row['email'] = v
                            elif k.startswith('website'):
                                row['website'] = v
                            elif k.startswith('last_traded_price'):
                                row['ltp'] = v
                            elif k.startswith('change'):
                                row['change'] = v
                            elif k.startswith('total_listed_shares'):
                                row['tls'] = v
                            elif k.startswith('paid_up_value'):
                                row['puv'] = v
                            elif k.startswith('total_paid_up_value'):
                                row['tpuv'] = v
                            elif k.startswith('closing_market_price'):
                                row['cmp'] = v
                            elif k.startswith('market_capitalization'):
                                row['mc'] = v
                            else:
                                row[k] = v
        return num_rows
    
    def get_prices(self, id_or_sym, sdate=False, edate=False, fields=None, limit=None):
        _id, sym = self._id_and_sym(id_or_sym)
        if _id:
            qs = {'stock-symbol': _id,
                  'startDate': sdate or PRICES_SDATE,
                  'endDate': edate or PRICES_EDATE,
                  }
            if limit is None:
                limit = self.limit
            prices = Nepse.get_instance('stockWisePrices', fields=fields, sess=self._sess)
            if prices.fetch(limit=limit, **qs):
                company_id = str(_id)
                for row in prices.rows:
                    if sym:
                        row['stock_symbol'] = sym
                    row['company_id'] = company_id
                return prices
            return None
        return False
    
    def _parse(self, tree, rows, xpath=None, func=None, xp=None, fn=None):
        if func is None:
            def _func(row, cols):
                url = _xp_href(row)
                cols['url'] = url
                _id = url.split('/')[-1]
                cols['id'] = _id
            func = _func
        return super(Company, self)._parse(tree, rows, xpath, func, xp, fn)
    
    def _id_and_sym(self, id_or_sym):
        if id_or_sym.isdigit():
            try:
                _id = int(id_or_sym)
            except ValueError:
                _id = False
            sym = False
        elif id_or_sym.isalpha():
            _id = False
            sym = id_or_sym.upper()
        else:
            _id = False
            sym = False
        if sym and (not self._fields or 'stock_symbol' in self._fields):
            num_rows = self.num_rows
            if not num_rows:
                qs = {'stock-symbol': sym}
                num_rows = self.fetch(FETCH_ALL, False, **qs)
            if num_rows:
                for row in self._rows:
                    if sym == row['stock_symbol']:
                        try:
                            _id = int(row['id'])
                        except ValueError:
                            _id = row['id']
                        break
        return _id, sym


class TodaysPrice(Nepse):
    
    _XPATH_AS_OF = "substring-after(%s/tr/td/label, 'As of ')"
    _XPATH_TOTAL = "normalize-space(%s/tr[contains(td, $name)]/td[2])"
    
    def __init__(self, fields=None, sess=None, **kwds):
        super(TodaysPrice, self).__init__('todaysprice', fields, sess, **kwds)
        self.date = kwds.get('date', None)
    
    def fetch(self, limit=None, date=None, **kwds):
        if date is None:
           date = self.date
        if date:
            kwds['startDate'] = date
        return super(TodaysPrice, self).fetch(limit, **kwds)
    
    def to_xml(self, fields=None, root='rows', rec='row', nl='\n'):
        r = super(TodaysPrice, self).to_xml(fields, root, rec, nl)
        data, attrs = self._data, []
        for attr in ['date', 'time', 'trans', 'quantity', 'amount']:
            value = data.get(attr, False)
            if value:
                attrs.append('%s="%s"' % (attr, value))
        if attrs:
            return r.replace('<%s' % root, '<%s %s' % (root, ' '.join(attrs)))
        return r
    
    def _init(self, tree):
        r = super(TodaysPrice, self)._init(tree)
        if r:
            data = self._data
            asof = tree.xpath(self._XPATH_AS_OF % self._xpath)
            data['date'], data['time'] = asof.split()
            xpath = etree.XPath(self._XPATH_TOTAL % self._xpath)
            data['trans'] = xpath(tree, name='Transactions')
            data['quantity'] = xpath(tree, name='Quantity')
            data['amount'] = xpath(tree, name='Amount')
        return r


class FloorSheet(Nepse):
    
    SCRAP_URL = 'https://www.merolagani.com/Floorsheet.aspx'
    
    _PREFIX = 'ctl00$ContentPlaceHolder1$'
    
    _INPUTS = {
                'symbol':   _PREFIX + 'ASCompanyFilter$txtAutoSuggest',
                'buyer':    _PREFIX + 'txtBuyerBrokerCodeFilter',
                'seller':   _PREFIX + 'txtSellerBrokerCodeFilter',
                'date':     _PREFIX + 'txtFloorsheetDateFilter',
                'search':   _PREFIX + 'lbtnSearchFloorsheet',
                'pagenum':  _PREFIX + 'PagerControl%d$hdnCurrentPage',
                'pagebtn':  _PREFIX + 'PagerControl%d$btnPaging',
                }
    
    _XPATHS = {
                'form':     "//form[contains(@action, 'Floorsheet')]//input",
                'rows':     "//div[@class='table-responsive']/table/tbody/tr",
                'cols':     'td',
                'pager':    "//div[@class='pagging'][%d]/div",
                'pages':    XPATH_BETWEEN % ("span[@class='hidden-xs']", ': ', ']'),
                'cpage':    "normalize-space(a[@class='current_page'])",
                'text':     'normalize-space()',
                }
    
    _PAGER_ID = 1
    
    def __init__(self, fields=None, sess=None, **kwds):
        super(FloorSheet, self).__init__('floorsheet', fields, sess, **kwds)
    
    def fetch(self, limit=None, sym='', date=None, **kwds):
        if sym.isalpha():
            kwds['stock-symbol'] = sym.upper()
        if date is None:
            return super(FloorSheet, self).fetch(limit, **kwds)
        if date:
            kwds['date'] = date
        return self.scrap(limit, kwds)
    
    def scrap(self, limit=None, qs=None):
        self.reset()
        data, inputs = {}, self._INPUTS
        if qs is not None:
            sym = qs.get('stock-symbol', '')
            if sym.isalpha():
                data[inputs['symbol']] = sym.upper()
            for k in ['buyer', 'seller']:
                v = qs.get(k, '')
                if v and (
                        v.isdigit() or (
                            '_' in v and
                            v.split('_')[0].isdigit()
                            )
                        ):
                    data[inputs[k]] = v
            date = qs.pop('date', False)
            if date:
                if '-' in date:
                    date = date.replace('-', '/')
                if date[4] == '/':
                    date = date.split('/')
                    date = '/'.join([date[1], date[2], date[0]])
                data[inputs['date']] = date
        num_cols = self._init(self._fetch(self._url, **qs))
        if not num_cols:
            return False
        xpaths = self._XPATHS
        xp_form = etree.XPath(xpaths['form'])
        xp_rows = etree.XPath(xpaths['rows'])
        xp_cols = etree.XPath(xpaths['cols'])
        xp_text = etree.XPath(xpaths['text'])
        fn = lambda x: xp_text(x).strip()
        page, pcid, = 1, self._PAGER_ID
        if limit:
            xp_pager = etree.XPath(xpaths['pager'] % pcid)
            xp_pages = etree.XPath(xpaths['pages'])
            xp_cpage = etree.XPath(xpaths['cpage'])
        elif qs:
            try:
                page = int(qs.get('page', 1))
            except ValueError:
                page = 1
        url = self.SCRAP_URL
        with requests.Session() as sess:
            sess.headers.update(USER_AGENT)
            resp = sess.get(url)
            sess.headers.update({'Referer': url})
            tree = html.fromstring(resp.content)
            forder, fcount = self._forder, self._fcount
            rows, num = [], 0
            while True:
                for e in xp_form(tree):
                    k, v = e.get('name'), e.get('value')
                    if k.startswith('__'):
                        data[k] = v if v else ''
                if page > 1:
                    data[inputs['pagenum'] % pcid] = page
                    data[inputs['pagebtn'] % pcid] = ''
                else:
                    data['__EVENTTARGET'] = inputs['search']
                    data['__EVENTARGUMENT'] = ''
                resp = sess.post(url, data=data)
                tree = html.fromstring(resp.content)
                for row in xp_rows(tree):
                    cols = xp_cols(row)
                    if len(cols) >= fcount:
                        cols = {f: fn(cols[i]) for f, i in forder}
                        rows.append(cols)
                num_rows = len(rows)
                if limit and (num_rows > num):
                    pager = xp_pager(tree)
                    if len(pager) >= 2:
                        pages = xp_pages(pager[0])
                        cpage = xp_cpage(pager[1])
                        try:
                            pages, cpage = int(pages), int(cpage)
                        except ValueError:
                            pages, cpage = 1, 1
                        if (page == cpage) and (cpage < pages):
                            num = num_rows
                            page += 1
                            continue
                break
        if num_rows:
            self._rows = rows
        return num_rows


get_instance = Nepse.get_instance

__all__ = [FETCH_DEF, FETCH_ALL, get_instance]


def main():
    
    def eprint(m):
        #print >> sys.stderr, m
        sys.stderr.write(m + '\n')
    
    def die(m='', n=1):
        if m:
            eprint(m)
        sys.exit(n)
    
    def usage(n=0):
        m = """nepse.py: Nepal Stock Exchange data scrap tool.

Usage: %s [OPTIONS] ARG [FIELDS] [QUERY]

OPTIONS:
    -a      Fetch all (only first page by default)
    -c      Output in CSV format (default is JSON)
    -d      Fetch details (only for companies)
    -f      Fetch latest floorsheet for given symbol. 
    -h      Print this usage help and exit.
    -p      Fetch price history for given symbol.
    -x      Output in XML format (default is JSON)

ARG:
    Object name or stock symbol.

FIELDS:
    Output fields (field1,field2,...)

QUERY:
    KEY=VALUE pairs seperated by space."""
        if n > 0:
            eprint(m % sys.argv[0])
        else:
            print(m % sys.argv[0])
        sys.exit(n)
    
    names = {
             'a':   'calculation',      # Trading Average Price
             'b':   'brokers',          # List of Brokers
             'c':   'company',          # Listed Companies
             'f':   'floorsheet',       # Today's Floor Sheet
             'i':   'indices',          # Datewise Indices
             'p':   'promoter-share',   # Promoter Share
             't':   'todaysprice',      # Today's Share Price
            }
    
    name = option = False
    limit = fields = None
    
    output = 'json'
    
    qs, args = {}, {}
    
    for arg in sys.argv[1:]:
        if arg.startswith('-'):
            if arg == '-h':
                usage(0)
            elif arg == '-a':
                limit = FETCH_ALL
            elif arg == '-d':
                args['detail'] = True
            elif arg == '-c':
                output = 'csv'
            elif arg == '-x':
                output = 'xml'
            elif arg in ['-f', '-p']:
                option = arg
            else:
                eprint('WARNING! Invalid option: %s' % arg)
        elif '=' in arg:
            k, v = arg.split('=')
            qs[k] = v
        elif ',' in arg:
            if fields is None:
                fields = [f for f in arg.split(',') if f]
            else:
                die('ERROR! FIELDS is given multiple times.')
        else:
            if not name:
                name = names.get(arg.lower(), arg)
            else:
                die('ERROR! NAME is given multiple times.')
    
    if name:
        with requests.Session() as session:
            if option and option == '-p': # stockWisePrices
                sdate, edate = qs.pop('sdate', False), qs.pop('edate', False)
                company = Company(sess=session, fields=['stock_symbol'], **args)
                nepse = company.get_prices(name, sdate, edate, limit=limit)
                if nepse:
                    num_rows, name = nepse.num_rows, 'stockWisePrices'
                else:
                    die('ERROR! Unable to fetch Prices for: %s' % name)
            else:
                if option and option == '-f': # FloorSheet
                    qs['stock-symbol'], name = name.upper(), 'floorsheet'
                nepse = get_instance(name, sess=session, **args)
                num_rows = nepse.fetch(limit, **qs)
            if num_rows:
                sys.stdout.write(nepse.result(output, fields or None))
            else:
                die('ERROR! Unable to fetch: %s' % name)
    else:
        # Print usage and exit.
        usage(1)


if __name__ == '__main__':
    main()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
