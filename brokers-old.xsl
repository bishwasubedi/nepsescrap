<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <brokers>
      <xsl:attribute name="url">
      	<xsl:value-of select="//table/tr/td/form/@action"/>
      </xsl:attribute>
      <xsl:apply-templates select="//table"/>
    </brokers>
  </xsl:template>

  <xsl:template match="table">
    <xsl:for-each select="tr[count(td)&gt;=6 and not(@*)]">
      <broker>
        <sn><xsl:value-of select="td[1]"/></sn>
        <code><xsl:value-of select="td[3]"/></code>
        <name><xsl:value-of select="td[2]"/></name>
        <address><xsl:value-of select="td[5]"/></address>
        <phone><xsl:value-of select="td[4]"/></phone>
      </broker>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
